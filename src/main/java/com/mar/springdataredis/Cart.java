package com.mar.springdataredis;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("cart")
public class Cart {

    @Id
    @Indexed
    private String id;
    private String item;
    private int quantity;
    @TimeToLive
    private long expiration;

    public Cart() {

    }

    public Cart(String id, String item, int quantity, long expiration) {
        this.id = id;
        this.item = item;
        this.quantity = quantity;
        this.expiration = expiration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getExpiration() {
        return expiration;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }
}
