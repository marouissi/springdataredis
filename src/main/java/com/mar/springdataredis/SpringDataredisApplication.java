package com.mar.springdataredis;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisKeyExpiredEvent;
import org.springframework.data.redis.core.RedisKeyValueAdapter.EnableKeyspaceEvents;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;


@SpringBootApplication
@EnableRedisRepositories(considerNestedRepositories = true, enableKeyspaceEvents = EnableKeyspaceEvents.ON_STARTUP)
public class SpringDataredisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataredisApplication.class, args);
	}


	@Bean
	ApplicationListener<RedisKeyExpiredEvent<Cart>> eventListener() {
		return event -> {
			System.out.println(String.format("Received expire event for key=%s with value %s.",
					new String(event.getSource()), event.getValue()));
		};
	}


}


interface ShoppingCartRepository extends CrudRepository<Cart, String> {

}


@Component
class StoreData implements ApplicationRunner {

	private ShoppingCartRepository repository;

	public StoreData(ShoppingCartRepository repository) {
		this.repository = repository;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		var saved = repository.save(new Cart("12", "item", 3, 60));
	}
}